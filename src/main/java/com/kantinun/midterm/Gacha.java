/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.midterm;

/**
 *
 * @author EAK
 */
public class Gacha extends Game {
    private int UR;
    private int SR;
    private int R;
    private int random;
    
    public Gacha(int cost){
        super(cost);
        this.UR = 5;
        this.SR = 10;
        this.R = 85;
    }
    
    public int AddMoney(int money){
        return money;
    }
    @Override
    public void print(){
        System.out.println("Welcome to Gacha Game!");
        System.out.println("Gacha rate");
        System.out.println("UR : " + UR + "% SR : " + SR + "% R : " + R + "%");
        System.out.println("1 time : " + cost + " gold, 10 times : " + (cost*(9)) + " gold");
    }
    
    public void Open(){
        if(HaveMoney(1)){
            this.money -= this.cost;
            Random();
            System.out.print("Result : "); Result(random);
            System.out.println();
        }else{
            NoMoney();
        }
    }
    
    //overload
    public void Open(int n){
        if(n > 10 || n <= 0) {
            System.out.println("Error : Please choose between 1-10 times");
        } else {
            if(!HaveMoney(n)){
                NoMoney();
            }else{
                if(n == 10){
                    this.money -= this.cost*9;
                }else{
                    this.money -= this.cost*n;
                }
                System.out.print("Result : ");
                for(int i=1; i<=n; i++){
                    Random();
                    Result(random);
                }
                System.out.println();
            }
        }
    }

    private void Result(int random) {
        if(random <= UR){
            System.out.print("UR  ");
        }else if(random > UR && random <= SR){
            System.out.print("SR  ");
        }else{
            System.out.print("R  ");
        }
    }
    
    private void Random() {
        random = (int)(Math.random()*100);
    }
    
    
    public boolean HaveMoney(int n){
        if(n == 10){
            return this.money >= cost*9;
        }else{
            return this.money >= cost*n;
        }
    }
    
    private void NoMoney() {
        System.out.println("Not enough money.");
    }
        
}
