/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.midterm;

import java.util.Scanner;
/**
 *
 * @author EAK
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Gacha gacha = new Gacha(100);
        
        System.out.println("Input your start money : ");
        gacha.money = kb.nextInt();
        System.out.println();
        
        gacha.print();
        
        System.out.println();
        while(true){
            System.out.println("You have " + gacha.money + " gold");
            int n = Input(kb);
            if(n == -1){
                System.out.println("Exit game.");
                break;
            }
            if(n == 1){
                gacha.Open();
            }else {
                gacha.Open(n);
            }
        }
        
    }
    
    public static int Input(Scanner kb){
        return kb.nextInt();
    }
    
}
